package io.gitlab.mguimard.openrgb;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


/**
 * Unit test for simple App.
 */
public class AppTest {
    @Test
    public void test() {
        assertTrue(true);
        String a = "4f 52 47 42 00 00 00 00 1a 04 00 00 0a 00 00 00 06 00 00 00 01 00 00 00 ff 00";
        String b = "4f 52 47 42 00 00 00 00 1a 04 00 00 0a 00 00 00 06 00 00 00 01 00 00 00 ff 00";
        assertEquals(a, b);
    }
}
